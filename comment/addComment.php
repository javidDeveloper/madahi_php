<?php
/**
 * Created by PhpStorm.
 * User: Javid
 * Date: 01/31/2019
 * Time: 07:10 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "../Connection.php";
include "../models/statusModel.php";
include "../models/musicModel.php";
$cnx = new Connection();
$musicID     = $_GET['musicID'];
$userID      = $_GET['userID'];
$comment     = $_GET['comment'];
$replayCommnet=0;
try {
    $cnx->addCommentToMusic($musicID,$userID,$comment);
    $statusModel = new  statusModel(array('value' => $comment, 'status' => "addedSuccessfully"));
    echo json_encode($statusModel);
} catch (Exception $e) {
    $statusModel = new  statusModel(array('value' => $comment, 'status' => $e));
    echo json_encode($statusModel);
}
?>
<!--http://localhost/v2/comment/addComment?musicID=1&userID=1&comment=testCommnet-->