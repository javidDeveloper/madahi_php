<?php
/**
 * Created by PhpStorm.
 * User: Javid
 * Date: 01/31/2019
 * Time: 06:16 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(0);

include "../Connection.php";
$cnx        = new Connection();
$page       = $_GET['page'];
$musicId    = $_GET['musicId'];
$len        = 70;
$result = $cnx->getCommentMusic( $page, $len,$musicId);
echo json_encode($result, JSON_UNESCAPED_SLASHES);

?>
<!--http://localhost/v2/comment/getComment?page=1&musicId=1-->