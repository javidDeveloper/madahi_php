<?php
/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/25/2019
 * Time: 11:39 PM
 */
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

include "../Connection.php";
$cnx = new Connection();
$page = $_GET['page'];
$len = 70;
$result = $cnx->getAllMusics($page, $len);
echo json_encode($result, JSON_UNESCAPED_SLASHES);

?>
<!--http://localhost/v2/music/getMusic?page=1-->

