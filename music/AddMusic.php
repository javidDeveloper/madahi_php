<?php
/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/29/2019
 * Time: 10:25 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "../Connection.php";
include "../models/statusModel.php";
//include "models/musicModel.php";
$cnx = new Connection();
$signerID    = $_GET['signerID'];
$cateID      = $_GET['cateID'];
$userID      = $_GET['userID'];
$title       = $_GET['title'];
$text        = $_GET['text'];
$urlDownload = $_GET['urlDownload'];
$urlAvatar   = $_GET['urlAvatar'];

try {
    $cnx->addMusic($signerID,$cateID,$userID,$title,$text,$urlDownload,$urlAvatar);
    $statusModel = new  statusModel(array('value' => $title, 'status' => "addedSuccessfully"));
    echo json_encode($statusModel);
    } catch (ErrorException $e) {
    $statusModel = new  statusModel(array('value' => $title, 'status' => $e));
    echo json_encode($statusModel);
    }
?>
<!--http://localhost/v2/music/AddMusic?signerID=1&cateID=1&userID=1&title=title&text=text&urlDownload=dlurl&urlAvatar=avurl-->
