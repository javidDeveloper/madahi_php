<?php
/**
 * Created by PhpStorm.
 * User: Javid
 * Date: 01/31/2019
 * Time: 09:49 PM
 */
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

include "../Connection.php";
$cnx = new Connection();
$musicId = $_GET['musicId'];
$result = $cnx->getCountLikeMusic($musicId);
echo json_encode($result);
?>
<!--http://localhost/v2/like/getCountLike?musicId=1-->