<?php
/**
 * Created by PhpStorm.
 * User: Javid
 * Date: 01/31/2019
 * Time: 08:59 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "../Connection.php";
include "../models/statusModel.php";
include "../models/musicModel.php";
$cnx = new Connection();

$musicID = $_GET['musicID'];
$userID = $_GET['userID'];
$likeMusic = $_GET['likeMusic'];
try {
    $result = $cnx->getStatusLikeMusic($userID, $musicID);
    if (intval($result) == 1) {
        $cnx->unLikeMusic($musicID, $userID);
        $statusModel = new  statusModel(array('value' => $musicID, 'status' => "unLiked"));
    } else if (intval($result) == 0){
        $cnx->likeMusic($musicID, $userID, $likeMusic);
        $statusModel = new  statusModel(array('value' => $musicID, 'status' => "liked"));
    }else{
        $cnx->likeMusic($musicID, $userID, $likeMusic);
        $statusModel = new  statusModel(array('value' => $musicID, 'status' => "liked"));
    }
    echo json_encode($statusModel);
} catch (Exception $e) {
    $statusModel = new  statusModel(array('value' => $musicID, 'status' => $e));
    echo json_encode($statusModel);
}
?>
<!--http://localhost/v2/like/addLike?musicID=1&userID=1&likeMusic=1-->