<?php
/**
 * Created by PhpStorm.
 * User: Javid
 * Date: 01/30/2019
 * Time: 11:38 PM
 */
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

include "../Connection.php";
$cnx = new Connection();
$singerId = $_GET['singerId'];
$result = $cnx->getSinger($singerId);
echo json_encode($result, JSON_UNESCAPED_SLASHES);
?>
<!--http://localhost/v2/singer/getSinger?singerId=1-->

