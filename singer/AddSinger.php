<?php
/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/25/2019
 * Time: 05:49 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include "../Connection.php";
include "../models/statusModel.php";
$cnx = new Connection();

$typeID = $_GET['typeID'];
$nameFamily = $_GET['nameFamily'];
$nationalCode = $_GET['nationalCode'];
$address = $_GET['address'];
$mobile = $_GET['mobile'];
$birthday = $_GET['birthday'];
$biography = $_GET['biography'];
$avatar = $_GET['avatar'];


try {
    if ($cnx->signerIsExist($nationalCode)) {
        $statusModel = new  statusModel(array('value' => $nationalCode, 'status' => "signerIsExist"));
    } else {
        $cnx->addSigner($typeID, $nameFamily, $nationalCode, $address, $mobile, $birthday, $biography, $avatar);
        $statusModel = new  statusModel(array('value' => $nationalCode, 'status' => "addedSuccessfully"));
    }
    echo json_encode($statusModel);
} catch (ErrorException $e) {
    $statusModel = new  statusModel(array('value' => $nationalCode, 'status' => $e));
    echo json_encode($statusModel);
}

?>
<!--http://localhost/v2/singer/AddSinger?typeID=1,2,3,4&nameFamily=javid&nationalCode=1236549870&address=tehran&mobile=09178516035&birthday=1397/12/12&biography=intehran&avatar=hfsdjkf-->