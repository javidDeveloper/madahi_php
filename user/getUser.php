<?php
/**
 * Created by PhpStorm.
 * User: Javid
 * Date: 01/30/2019
 * Time: 11:04 PM
 */
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

include "../Connection.php";
$cnx = new Connection();
$userName = $_GET['userName'];
$result = $cnx->getUser($userName);
echo json_encode($result);
?>
<!--http://localhost/v2/user/getUser?userName=admin-->

