<?php
/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/25/2019
 * Time: 05:49 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "../Connection.php";
include "../models/statusModel.php";
$cnx = new Connection();
$nameFamily = $_GET['nameFamily'];
$mobile     = $_GET['mobile'];
$email      = $_GET['email'];
$userName   = $_GET['userName'];
$password   = $_GET['password'];
$avatar     = $_GET['avatar'];

try {
    if ($cnx->userIsExist($userName)) {
        $statusModel = new  statusModel(array('value' => $userName, 'status' => "userIsExist"));
    } else {
        $cnx->addUser($nameFamily, $mobile, $email, $userName, $password, $avatar);
        $statusModel = new  statusModel(array('value' => $userName, 'status' => "addedSuccessfully"));

    }
    echo json_encode($statusModel);
} catch (ErrorException $e) {
    $statusModel = new  statusModel(array('value' => $userName, 'status' => $e));
    echo json_encode($statusModel);
}
?>
<!--http://localhost/v2/user/AddUser?nameFamily=javid%sattar&mobile=09178516035&email=sampleEmail@email.com&userName=j.sattar&password=123&avatar=https://www.imsa-search.com/wp-content/uploads/2018/06/avatar-300x300.png-->
