<?php

/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/25/2019
 * Time: 03:57 PM
 */
Class Connection
{
    private $userName = "root";
    private $password = "";
    private $server = "localhost";
    private $databaseName = "madahi";
    private $connection;

    // Insert Query
    private $addMusic = "INSERT INTO music( signerID, cateID, userID,  title, text,  urlDownload, urlAvatar) VALUES (?,?,?,?,?,?,?)";
    private $addUser = "INSERT INTO users(nameFamily, mobile, email, userName, password, avatar) VALUES (?,?,?,?,?,?)";
    private $addSigner = "INSERT INTO singer(typeID,nameFamily,nationalCode,address,mobile,birthday,biography,avatar) VALUES (?,?,?,?,?,?,?,?)";
    private $addCommentMusic = "INSERT INTO comment_music( musicID,userID,comment) VALUES (?,?,?)";
    private $addLikeMusic = "INSERT INTO like_music(musicID, userID, likeMusic) VALUES (?,?,?)";
    private $addCatMusic = "INSERT INTO cat_music(typeID, title) VALUES (?,?)";
    private $addTypeMusic = "INSERT INTO type_music(Title) VALUES (?)";


    // Select Query
    private $getAllMusic = "SELECT mu.musicID as id, sing.nameFamily as singerName, cat.titleCate  as category, usr.nameFamily as sender, mu.title as title, mu.statusID as status, mu.text as text, mu.urlDownload as urlMusic, mu.urlAvatar as urlAvatar, mu.dateInsert as date FROM music mu, singer sing, cat_music cat, users usr WHERE mu.signerID = sing.signerID and mu.cateID = cat.cateID AND mu.userID = usr.userID and statusID = 3 ORDER BY musicID DESC limit :start,:len ";
    private $getUserInfo = "SELECT * FROM users WHERE userName = ?";
    private $getUserExist = "SELECT count(*) from users where userName = ?";
    private $getSignerExist = "SELECT count(*) from singer where nationalCode = ?";
    private $getSinger = "SELECT * FROM singer WHERE signerID = ?";
    private $getCommentMusic = "SELECT cm.commentID, mu.musicID, usr.nameFamily, usr.avatar, cm.replayCommnet, cm.comment, cm.dataComment FROM comment_music cm, music mu, users usr WHERE mu.musicID = cm.musicID and cm.userID = usr.userID and cm.musicID = :m_id and cm.statusID = 3 ORDER BY cm.commentID DESC limit :start,:len";
    private $getCountLikeMusic = "SELECT count(*) as count from like_music where musicID = ?";
    private $getStatusLikeMusic = "SELECT likeMusic FROM like_music WHERE userID = ? and musicID = ?";


    // Delete Or Update Query
    private $deleteLikeMusic = "DELETE FROM like_music WHERE musicID = ? and userID = ?";

    function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host=$this->server;dbname=$this->databaseName;charset=utf8", $this->userName, $this->password,
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                )
            );
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /** user */
    function addUser($nameFamily, $mobile, $email, $userName, $password, $avatar)
    {
        $prepareResult = $this->connection->prepare($this->addUser);
        $prepareResult->execute(array($nameFamily, $mobile, $email, $userName, $password, $avatar));
    }

    function getUser($userName)
    {
        $prepareResult = $this->connection->prepare($this->getUserInfo);
        $prepareResult->execute([$userName]);
        return $prepareResult->fetch(PDO::FETCH_ASSOC);
    }

    function userIsExist($userName)
    {
        $prepareResult = $this->connection->prepare($this->getUserExist);
        $prepareResult->execute([$userName]);
        $count = $prepareResult->fetchColumn();
        if ($count == 0)
            return false;
        else
            return true;
    }

    /** singer */
    function addSigner($typeID, $nameFamily, $nationalCode, $address, $mobile, $birthday, $biography, $avatar)
    {
        $prepareResult = $this->connection->prepare($this->addSigner);
        $prepareResult->execute(array($typeID, $nameFamily, $nationalCode, $address, $mobile, $birthday, $biography, $avatar));
    }

    function getSinger($singerId)
    {
        $prepareResult = $this->connection->prepare($this->getSinger);
        $prepareResult->execute([$singerId]);
        return $prepareResult->fetch(PDO::FETCH_ASSOC);
    }

    function signerIsExist($nationalCode)
    {
        $prepareResult = $this->connection->prepare($this->getSignerExist);
        $prepareResult->execute([$nationalCode]);
        $count = $prepareResult->fetchColumn();
        if ($count == 0)
            return false;
        else
            return true;
    }

    /** music */
    function addMusic($signerID, $cateID, $userID, $title, $text, $urlDownload, $urlAvatar)
    {
        $prepareResult = $this->connection->prepare($this->addMusic);
        $prepareResult->execute(array($signerID, $cateID, $userID, $title, $text, $urlDownload, $urlAvatar));
    }


    function getAllMusics($page, $len)
    {
        $start = ($page - 1) * $len;
        $preparedResult = $this->connection->prepare($this->getAllMusic);
        $preparedResult->bindValue(':start', intval($start), PDO::PARAM_INT);
        $preparedResult->bindValue(':len', intval($len), PDO::PARAM_INT);
        $preparedResult->execute();
        return $preparedResult->fetchAll(PDO::FETCH_ASSOC);
    }

    /** comment */
    function getCommentMusic($page, $len, $musicId)
    {
        $start = ($page - 1) * $len;
        $preparedResult = $this->connection->prepare($this->getCommentMusic);
        $preparedResult->bindValue(':m_id', intval($musicId), PDO::PARAM_INT);
        $preparedResult->bindValue(':start', intval($start), PDO::PARAM_INT);
        $preparedResult->bindValue(':len', intval($len), PDO::PARAM_INT);
        $preparedResult->execute();
        return $preparedResult->fetchAll(PDO::FETCH_ASSOC);
    }

    function addCommentToMusic($musicID, $userID, $comment)
    {
        $prepareResult = $this->connection->prepare($this->addCommentMusic);
        $prepareResult->execute(array($musicID, $userID, $comment));
    }

    /** like */
    function getStatusLikeMusic($userID, $musicID)
    {
        $preparedResult = $this->connection->prepare($this->getStatusLikeMusic);
        $preparedResult->execute(array($userID, $musicID));
        return $preparedResult->fetch(PDO::FETCH_ASSOC);
    }

    function getCountLikeMusic($musicId)
    {
        $preparedResult = $this->connection->prepare($this->getCountLikeMusic);
        $preparedResult->execute([$musicId]);
        return $preparedResult->fetch(PDO::FETCH_ASSOC);
    }

    function likeMusic($musicID, $userID, $likeMusic)
    {
        $prepareResult = $this->connection->prepare($this->addLikeMusic);
        $prepareResult->execute(array($musicID, $userID, $likeMusic));
    }

    function unLikeMusic($musicID, $userID)
    {
        $prepareResult = $this->connection->prepare($this->deleteLikeMusic);
        $prepareResult->execute(array($musicID, $userID));
    }
}

?>