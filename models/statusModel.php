<?php
/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/25/2019
 * Time: 06:42 PM
 */

class statusModel implements JsonSerializable

{
    protected  $value;
    protected  $status;

    /**
     * statusModel constructor.
     * @param $value
     * @param $status
     */
    public function __construct(array $data)
    {
        $this->value = $data['value'];
        $this->status = $data['status'];
    }


    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return
            [
                'value'   => $this->getValue(),
                'statue' => $this->getStatus()
            ];
    }
}