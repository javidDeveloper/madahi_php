<?php
/**
 * Created by PhpStorm.
 * User: javid
 * Date: 01/29/2019
 * Time: 10:41 PM
 */

class musicModel implements JsonSerializable
{
protected $signerID;
protected $cateID;
protected $userID;
protected $status;
protected $title;
protected $text;
protected $urlDownload;
protected $urlAvatar;

    /**
     * musicModel constructor.
     * @param $signerID
     * @param $cateID
     * @param $userID
     * @param $status
     * @param $title
     * @param $text
     * @param $urlDownload
     * @param $urlAvatar
     */
    public function __construct(array $data)
    {
        $this->signerID     = $data['signerID'];
        $this->cateID       = $data['cateID'];
        $this->userID       = $data['userID'];
        $this->status       = $data['status'];
        $this->title        = $data['title'];
        $this->text         = $data['text'];
        $this->urlDownload  = $data['urlDownload'];
        $this->urlAvatar    = $data['urlAvatar'];
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return
            [
                'signerID'    => $this->getUserID(),
                'cateID'      => $this->getCateID(),
                'userID'      => $this->getUserID(),
                'status'      => $this->getStatus(),
                'title'       => $this->getTitle(),
                'text'        => $this->getText(),
                'urlDownload' => $this->getUrlDownload(),
                'urlAvatar'   => $this->getUrlAvatar()
            ];
    }

    /**
     * @return mixed
     */
    public function getSignerID()
    {
        return $this->signerID;
    }

    /**
     * @return mixed
     */
    public function getCateID()
    {
        return $this->cateID;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getUrlDownload()
    {
        return $this->urlDownload;
    }

    /**
     * @return mixed
     */
    public function getUrlAvatar()
    {
        return $this->urlAvatar;
    }

}